%bcond_without python2

Name:		python-hwdata
Version:	2.4.1
Release:	1
Summary:	Python bindings to hwdata package
License:	GPLv2
URL:		https://github.com/xsuchy/python-hwdata
Source0:	https://github.com/xsuchy/python-hwdata/archive/%{name}-%{version}-1.tar.gz

BuildArch:  	noarch

%description
Provide python interface to database stored in hwdata package.
It allows you to get human readable description of USB and PCI devices.

%package -n python3-hwdata
Summary:	Python bindings to hwdata package

BuildRequires:	python3-devel
Requires:	hwdata

%{?python_provide:%python_provide python3-hwdata}

%description -n python3-hwdata
Provide python interface to database stored in hwdata package.
It allows you to get human readable description of USB and PCI devices.

%package_help
%prep
%autosetup -n %{name}-%{name}-%{version}-1 -p1
rm -rf %{py3dir}
cp -a . %{py3dir}

%build
pushd %{py3dir}
%_bindir/python3 setup.py build '--executable=%_bindir/python3 -s'
popd

%install
pushd %{py3dir}
%_bindir/python3 setup.py install -O1 --skip-build --root %buildroot
popd

%check


%files -n python3-hwdata
%defattr(-,root,root)
%license LICENSE
%{python3_sitelib}/*

%files help
%defattr(-,root,root)
%doc example.py README.md html

%changelog
* Thu Jan 11 2024 Paul Thomas <paulthomas100199@gmail.com> - 2.4.1-1
- update to version 2.4.1

* Fri Apr 07 2023 wulei <wu_lei@hoperun.com> - 2.3.8-1
- Upgrade package to version 2.3.8

* Mon Nov 2 2020 wangjie<wangjie294@huawei.com> -2.3.7-8
- Type:NA
- ID:NA
- SUG:NA
- DESC:remove python2

* Mon Sep 7 2020 Ge Wang<wangge20@huawei.com> - 2.3.7-7
- Modify Source0 Url

* Fri Jun 19 2020 fuyangqing<fuyangqing@huawei.com> - 2.3.7-6
-Type:bugfix
-ID:NA
-SUG:NA
-DESC:change sepc hardcode the dependent python version number

* Tue Feb 11 2020 huzunhao<huzunhao2@huawei.com> - 2.3.7-5
- Package init
